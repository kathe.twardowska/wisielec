﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ClassLibrary
{
    public class myRandom
    {
        public static void selectRandomComboBoxItem(ComboBox comboBox)
        {
            Random rnd = new Random();
            comboBox.SelectedIndex = rnd.Next(comboBox.Items.Count);
        }
    }
}
