﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Files
    {
        private static string _filePath = getWordsPath();

        private static string getWordsPath()
        {
            string wordsPath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
            wordsPath = Directory.GetParent(Directory.GetParent(wordsPath).FullName).FullName;
            wordsPath = Path.Combine(wordsPath, "Resources", "Words");
            return wordsPath;
        }

        public static List<string> getCategories()
        {
            List<string> filenames = Directory.GetFiles(_filePath, "*.txt")
                                     .Select(Path.GetFileNameWithoutExtension)
                                     .ToList();
            return filenames;
        }

        public static List<string> getWords(string category = "")
        {
            if(category.Length == 0 || category.Equals("Wszystkie"))
            {
                List<string> temp = new List<string>();
                foreach(string cat in getCategories())
                {
                    foreach(string word in getWords(cat))
                    {
                        temp.Add(word);
                    }
                }
                temp.Sort();
                return temp;
            }
            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(Path.Combine(_filePath, category+".txt"),Encoding.Default))
                {
                    
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd().Replace(Environment.NewLine, "");
                    List<string> list = line.Split(',').ToList();
                    list.Sort();
                    return list;
                }
            }
            catch (IOException e)
            {
                
                return new List<string>() { e.Message };
            }
        }
    }
}
