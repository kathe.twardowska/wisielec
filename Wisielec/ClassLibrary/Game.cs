﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace ClassLibrary
{
    public class Game
    {
        Grid hangmanGrid, passwordGrid;
        Image win;
        string word = "";
        string answerlist = "";
        bool playing = false;
        int lives = 0;
        int guessed = 0;

        public Game(Grid hangman, Grid password, Image win)
        {
            hangmanGrid = hangman;
            passwordGrid = password;
            this.win = win;
        }

        public void startGame(string word)
        {
            playing = true;
            lives = 10;
            guessed = 0;
            this.word = word;
            answerlist = "";
            generatePasswordGrid();
            prepareHangman();
        }

        private void generatePasswordGrid()
        {
            passwordGrid.RowDefinitions.Clear();
            passwordGrid.Children.Clear();
            List<string> temp = new List<string>();
            foreach (string s in word.Split(' '))
            {
                temp.Add(s);
                temp.Add(" ");
            }
            temp.RemoveAt(temp.Count - 1);

            for (int j = 0; j < temp.Count; j++)
            {
                passwordGrid.RowDefinitions.Add(new RowDefinition());
                Grid g = new Grid();
                Grid.SetRow(g,j);

                for (int i = 0; i < temp[j].Length; i++)
                {
                    g.ColumnDefinitions.Add(new ColumnDefinition());
                    Label label = new Label();
                    label.Content = "\u25A0";
                    //label.Content = temp[j][i];
                    label.VerticalContentAlignment = VerticalAlignment.Center;
                    label.HorizontalContentAlignment = HorizontalAlignment.Center;
                    label.Tag = temp[j][i];
                    label.Foreground = Brushes.Red;
                    label.FontWeight = FontWeights.Bold;
                    label.FontSize = 40;
                    Grid.SetColumn(label, i);
                    g.Children.Add(label);
                }
                passwordGrid.Children.Add(g);
            }
        }

        private void prepareHangman()
        {
            foreach(Image i in hangmanGrid.Children)
            {
                i.BeginAnimation(Image.OpacityProperty,new DoubleAnimation(0,new TimeSpan(0,0,2)));
            }
        }

        public string answer(char letter)
        {
            if (answerlist.Contains(letter) || !playing)
                return answerlist;
            answerlist += letter;
            if (word.ToLower().Contains(letter.ToString().ToLower()))
            {
                goodAnswer(letter);
            }
            else
            {
                badAnswer();
            }
            if (guessed == word.Length)
                gameWon();
            if (lives <= 0)
                gameOver();
            return answerlist;
        }

        private void goodAnswer(char letter)
        {
            foreach(Grid g in passwordGrid.Children)
            {
                foreach(Label l in g.Children)
                {
                    if (l.Tag.ToString().ToLower() == letter.ToString().ToLower())
                    {
                        l.Content = letter;
                        guessed++;
                    }
                }
            }
        }

        private void badAnswer()
        {
            ((Image)hangmanGrid.Children[10-lives]).BeginAnimation(Image.OpacityProperty, new DoubleAnimation(1d, new TimeSpan(0, 0, 2)));
            lives--;
        }

        private void gameOver()
        {
            playing = false;
            MessageBox.Show("YOU DIED");
        }

        private void gameWon()
        {
            win.Visibility = Visibility.Visible;
            playing = false;
        }
    }
}
