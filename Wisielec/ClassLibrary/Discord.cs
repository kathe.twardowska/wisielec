﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Reflection;
using Discord;
using Discord.WebSocket;
using Discord.Webhook;
using Discord.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace ClassLibrary
{
    public class Discord
    {
        private CommandService commands;
        private DiscordSocketClient client;
        private IServiceProvider services;
        Random rand;

        string token = "NTM3NzE1OTc4MjM5MjEzNTY5.D0JAow.Qz-1H5durAM3kB3sAmZobMzcQA0";

        public async Task Start()
        {
            
            commands = new CommandService();
            client = new DiscordSocketClient();
            services = new ServiceCollection().BuildServiceProvider();

            await InstallCommands();
            client.LoginAsync(TokenType.Bot, token);
            while(client.LoginState != LoginState.LoggedIn)
            {
                Console.WriteLine(client.LoginState);
            }
            await client.StartAsync();
            client.MessageReceived += Client_MessageReceived;
            
            client.UserJoined += UserJoined;

            //await Task.Delay(-1);


        }

        private Task Client_MessageReceived(SocketMessage msg)
        {
            throw new NotImplementedException();
        }

        public async Task InstallCommands()
        {
            client.MessageReceived += HandleCommand;
            await commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        public async Task HandleCommand(SocketMessage msgPar)
        {
            var msg = msgPar as SocketUserMessage;
            char prefix = '*';
            if (msg == null) return;

            int argPos = 0;
            if (!(msg.HasCharPrefix(prefix, ref argPos) || msg.HasMentionPrefix(client.CurrentUser, ref argPos))) return;
            var context = new CommandContext(client, msg);
            var result = await commands.ExecuteAsync(context, argPos, services);
        }

        public async Task UserJoined(SocketGuildUser user)
        {
            await user.SendMessageAsync("Buaaa");
        }
    }
}
