﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClassLibrary;

namespace Wisielec
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Game game;

        public MainWindow()
        {
            InitializeComponent();
            DiscordConnect();
            Load();
        }

        static void DiscordConnect() => new Discord().Start().GetAwaiter().GetResult();
        private void Load()
        {
            List<string> categories = Files.getCategories().OrderByDescending(x => x).ToList();
            categories.Add("Wszystkie");
            categories.Reverse();
            comboBoxCategory.Dispatcher.Invoke(new Action(()=> {
                comboBoxCategory.ItemsSource = categories;
                comboBoxCategory.SelectedIndex = 0;
            }));
            comboBoxWord.Dispatcher.Invoke(new Action(() => {
                comboBoxWord.ItemsSource = Files.getWords();
                comboBoxWord.SelectedIndex = 0;
            }));
            game = new Game(hangmanProgress, haslo, win);

        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            comboBoxWord.ItemsSource = Files.getWords(comboBox.SelectedItem.ToString());
            comboBoxWord.SelectedIndex = 0;
        }

        private void Cbi1_Selected(object sender, RoutedEventArgs e)
        {

        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            myRandom.selectRandomComboBoxItem(comboBoxCategory);
        }

        private void Image_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            myRandom.selectRandomComboBoxItem(comboBoxWord);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(button.Content.Equals("Start"))
            {
                game.startGame(comboBoxWord.SelectedItem.ToString());
                button.Content = "Restart";
                label.Visibility = Visibility.Visible;
                button1.Visibility = Visibility.Visible;
                haslo.Visibility = Visibility.Visible;
                comboBoxWord.Visibility = Visibility.Hidden;
                label1.Visibility = Visibility.Visible;
            }
            else
            {
                foreach (Image i in hangmanProgress.Children)
                {
                    i.BeginAnimation(Image.OpacityProperty, new DoubleAnimation(1, new TimeSpan(0, 0, 2)));
                }
                haslo.Visibility = Visibility.Hidden;
                win.Visibility = Visibility.Hidden;
                label.Visibility = Visibility.Hidden;
                button1.Visibility = Visibility.Hidden;
                comboBoxWord.Visibility = Visibility.Visible;
                button.Content = "Start";
                label2.Content = "";
                label1.Visibility = Visibility.Visible;
            
            }
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            label2.Content = game.answer(label.Content.ToString().Split(' ')[1][0]);
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.RightAlt))
            {
                switch (e.Key)
                {
                    case Key.E:
                        label.Content = "Litera: Ę";
                        return;
                    case Key.O:
                        label.Content = "Litera: Ó";
                        return;
                    case Key.A:
                        label.Content = "Litera: Ą";
                        return;
                    case Key.S:
                        label.Content = "Litera: Ś";
                        return;
                    case Key.L:
                        label.Content = "Litera: Ł";
                        return;
                    case Key.Z:
                        label.Content = "Litera: Ż";
                        return;
                    case Key.X:
                        label.Content = "Litera: Ź";
                        return;
                    case Key.C:
                        label.Content = "Litera: Ć";
                        return;
                    case Key.N:
                        label.Content = "Litera: Ń";
                        return;
                }
            }
            if (e.Key >= Key.A && e.Key <= Key.Z)
                label.Content = "Litera: "+e.Key;
        }
    }
}
